import * as React from 'react';
import {Route, Router, Switch} from 'react-router-dom';
import history from './history';

import {Account} from '../App/Account';
import {App} from '../App/App';
import {Unit} from '../App/Unit';
import {User} from '../App/User';

import NotFound from '../App/NotFound';

// const Routes = () => (
class Routes extends React.Component {

  static childContextTypes = {
    history: React.PropTypes.array
  };

  getChildContext () {
    return {
      history: history
    }
  }

  render() {
    return <Router history={history}>
      <div>
        <Switch>
          <Route path="/" component={App} exact={true}/>
          <Route path="/accounts" component={Account}/>
          <Route path="/units" component={Unit}/>
          <Route path="/units/:id" component={Unit}/>
          <Route path="/users/:id" component={User}/>
          <Route path ="*" component={NotFound}/>
        </Switch>
      </div>
    </Router>
  }
}

export default Routes
