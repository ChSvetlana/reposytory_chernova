import * as React from 'react';
import {Actions} from '../Actions/Actions';

interface IAppProps {
  loginStatus: boolean;
  loading: boolean;
  isCorrectLogin: boolean;
  loginValue: string;
  handleLogout: any;
  changeHandler: any;
  handleSubmit: any;
  goToAccountPage: any;
  actions: Actions;
}

function ContainerApp(props: IAppProps) {
    const {loginValue, loading} = props;
    return (
            <div >
                <h3>
                    Задание по отображению информации по организациям
                </h3>

                <form className="col-md-4" onSubmit={props.handleSubmit}>
                    <input className="input-login" type="text" placeholder="Введите логин" value={loginValue} onChange={props.changeHandler}/>
                    <input className="btn btn-outline-primary" disabled={loading} type="submit" value="Войти"/>
                </form>

                {
                    !props.isCorrectLogin ? <p>Некорректный логин. Попробуйте еще раз</p> :
                    props.loading ?
                        <p>Авторизация...</p> :
                        props.loginStatus ?
                            <div>
                                <p> Авторизация прошла успешно. Выберите дальнейшее действие.
                                <input className="btn btn-outline-warning" disabled={loading} type="button" value="Выйти" onClick={props.handleLogout}/>
                                <input className="btn btn-outline-warning" disabled={loading} type="button" value="Просмотр организаций" onClick={props.goToAccountPage}/></p>
                            </div>
                            :

                            <div className= "col-md-12">
                                <p>
                                    Ошибка. В случае повторения ошибки обратитесь в службу технической поддержки.
                                </p>
                            </div>

                }

                </div>
            );
  }

export default ContainerApp;
