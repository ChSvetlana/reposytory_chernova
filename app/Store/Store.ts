import {Action, applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';
import {IAccount} from '../App/Account';
import  {IUnit} from '../App/Unit';
import  {IUser} from '../App/User';

export interface IActionType extends Action {
  type: string;
  payload: any;
}

export interface IStoreState {
  data?: number;
  loginStatus: string;
  loading: boolean;
  accounts?: Array<IAccount>;
  units: Array<IUnit>;
  users: Array<IUser>;
  orgId: string,
  unitId: string,
  counter: number,
  error: string,
}

const initialState = {
  get state(): IStoreState {
    return {
      loginStatus: null,
      loading: false,
      accounts: null,
      units: null,
      users: null,
      orgId: null,
      unitId: null,
      counter: 100,
      error: null,
    }
  }
};

function reducer (state: IStoreState = initialState.state, action: IActionType) {

  let newAccounts: IAccount[];
  let newUnits: IUnit[];
  let newUsers: IUser[];

  switch (action.type) {

    //загрузка данных
    case AsyncActionTypes.LOGIN_BEGIN_LOADING || AsyncActionTypes.LOAD_ORGANISATION_BEGIN || AsyncActionTypes.LOAD_UNIT_BEGIN || AsyncActionTypes.LOAD_USER_BEGIN:
    return {
      ...state,
      loading: true,
      error: null,
    };

    //авторизация
    case `${ActionTypes.LOGIN}${AsyncActionTypes.LOGIN_SUCCESS}`:
    return {
      ...state,
      loginStatus: action.payload,
      loading: false,
    };

    //ошибка
    case AsyncActionTypes.LOGIN_FAILURE || AsyncActionTypes.LOAD_ORGANISATION_FAILURE || AsyncActionTypes.LOAD_UNIT_FAILURE || AsyncActionTypes.LOAD_USER_FAILURE:
    return {
      ...state,
      loading: false,
      error: action.payload,
    };

    //выход
    case ActionTypes.LOGOUT:
    return {
      ...state,
      loginStatus: null,
      error: null,
      loading: false,
    };

    //получение организаций
    case `${ActionTypes.GET_ACCOUNTS}${AsyncActionTypes.LOAD_ORGANISATION_SUCCESS}`:
      if (state.accounts) {
        return {
          ...state
        }
      }
    return {
      ...state,
      accounts: action.payload,
    };

    //получение подразделений
    case `${ActionTypes.GET_UNITS}${AsyncActionTypes.LOAD_UNIT_SUCCESS}`:
    return {
      ...state,
      units: action.payload,
    };

    //получение пользователей
    case `${ActionTypes.GET_USERS}${AsyncActionTypes.LOAD_USER_SUCCESS}`:
      return {
        ...state,
        users: action.payload,
    };

    //установка текущей организации
    case ActionTypes.SET_CURRENT_ORG:
      return {
        ...state,
        orgId: action.payload,
      };

    //установка текущего подразделения
    case ActionTypes.SET_CURRENT_UNIT:
      return {
        ...state,
        unitId: action.payload,
      };

    //запись новой организации
    case ActionTypes.SAVE_ORG:
      state.accounts.push(action.payload);
      return {
        ...state,
        accounts: state.accounts,
      };

    //редактирование организации
    case ActionTypes.EDIT_ORG:
      newAccounts = state.accounts;
      if (!newAccounts.some((org) => {
        if(org.id === action.payload.id) {
          org.name = action.payload.name;
          org.inn = action.payload.inn;
          org.address = action.payload.address;
          return true
        }
        return false
        })) {
        return {...state}
      }
      return {
        ...state,
        accounts: newAccounts,
      };

    //редактирование подразделения
    case ActionTypes.EDIT_UNIT:
      newUnits = state.units;
      if (!newUnits.some((unit) => {
        if (unit.id === action.payload.id) {
          unit.name = action.payload.name;
          unit.telefon = action.payload.telefon;
          return true
        }
        return false
        })) {
        return {...state}
      }
      return {
        ...state,
        units: newUnits,
      };

    //счетчик, используется для установки уникальных ид орг-ций, юзеров, подразделений
    case ActionTypes.INCREMENT_COUNTER:
      return {
        ...state,
        counter: ++state.counter,
      };

  //редактирование пользователя
    case ActionTypes.EDIT_USER:
    newUsers = state.users;
    if (!newUsers.some((user) => {
      if (user.id === action.payload.id) {
        user.fio = action.payload.fio;
        user.address = action.payload.address;
        user.job = action.payload.job;
        return true
      }
      return false
      })) {
      return {...state}
    }

  return {
    ...state,
    users: newUsers,
  };

    //удаление организации
    case ActionTypes.DELETE_ORG:
      newAccounts = state.accounts;
      if (!newAccounts.some((org, i) => {
        if(org.id === action.payload.id) {
    newAccounts.splice(i, 1);
          return true
        }
        return false
      })) {
        return {...state}
      }

      return {
        ...state,
        accounts: newAccounts,
      };

  //запись нового подразделения
  case ActionTypes.SAVE_UNIT:
  state.units.push(action.payload);
  return {
    ...state,
    units: state.units,
  };

  //запись нового пользователя
  case ActionTypes.SAVE_USER:
  state.users.push(action.payload);
  return {
    ...state,
    users: state.users,
  };

    //удаление подразделения
  case ActionTypes.DELETE_UNIT:
  newUnits = state.units;
  if (!state.units.some((unit, i) => {
    if(unit.id === action.payload.id) {
      newUnits.splice(i, 1);
      return true
    }
    return false
    })) {
    return {...state}
  }

  return {
    ...state,
    units: newUnits,
  };

    //удаление пользователя
  case ActionTypes.DELETE_USER:
  newUsers = state.users;
  if (!state.users.some((user, i) => {
    if(user.id === action.payload.id) {
      newUsers.splice(i, 1);
      return true
    }
    return false
    })) {
    return {...state}
  }

  return {
    ...state,
    users: newUsers,
  };

  }
  return state
  }

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
