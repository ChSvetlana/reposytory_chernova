import {Dispatch} from 'redux';
//import {data} from '../app';
import {IAccount} from '../App/Account';
import {IUnit} from '../App/Unit';
import {IUser} from '../App/User';
import {ActionTypes, AsyncActionTypes} from './Consts';

export interface IDispatchProps {
  actions: Actions;
}

export class Actions {
  constructor(private dispatch: Dispatch<IDispatchProps>) {
  }

  onClick = (i: number) => this.dispatch({type: ActionTypes.CLICK, payload: i});

  onLogin = (login: string, password: string) => {
    this.dispatch({type: AsyncActionTypes.LOGIN_BEGIN_LOADING});
    //this.dispatch((dispatch: Dispatch<IDispatchProps>) => {

    //Простейший асинхронный экшен
    // setTimeout(() => {
    //   dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
    // }, 2000)

    //Экшен для запроса к РЕСТам
    //fetch('http://www.mocky.io/v2/5aafaf2c2d000048006eff2c') //404
    fetch('http://www.mocky.io/v2/5b81ad6c3400006300ecb3ac') //200 - true
    //fetch('http://www.mocky.io/v2/5aafafa32d000056006eff3b') //200 - false
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then(data => {
        //формат ответа:
        //{"password": "1234"}
        if (data.password === password) {
          this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.LOGIN_SUCCESS}`, payload: login})
        } else {
          this.dispatch({type: AsyncActionTypes.LOGIN_FAILURE, payload: 'Неверный пароль'})
        }
      })
      .catch(error => {
        console.log(error);
        this.dispatch({type: AsyncActionTypes.LOGIN_FAILURE, payload: 'Сервер недоступен'});
      });
    //});
  };

  onLogout = () => this.dispatch({type: ActionTypes.LOGOUT});

  getAccounts = () => {
    this.dispatch({type: AsyncActionTypes.LOAD_ORGANISATION_BEGIN});
    fetch(' http://www.mocky.io/v2/5b6a7f2f3200004c00372f01')
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw 'error';
        }
      })
      .then(accounts => {
          this.dispatch({type: `${ActionTypes.GET_ACCOUNTS}${AsyncActionTypes.LOAD_ORGANISATION_SUCCESS}`, payload: accounts});
        }
      )
      .catch(error => {
        this.dispatch({type: AsyncActionTypes.LOAD_ORGANISATION_FAILURE, payload: error});
      });
  };

  getUnits = () => {
    this.dispatch({type: AsyncActionTypes.LOAD_UNIT_BEGIN});
    fetch('http://www.mocky.io/v2/5b7d549d33000074004a0266')
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw 'error';
        }
      })
      .then(units => {
          this.dispatch({type: `${ActionTypes.GET_UNITS}${AsyncActionTypes.LOAD_UNIT_SUCCESS}`, payload: units});
        }
      )
      .catch(error => {
        this.dispatch({type: AsyncActionTypes.LOAD_UNIT_FAILURE, payload: error});
      })
  };

  getUsers = () => {
    this.dispatch({type: AsyncActionTypes.LOAD_USER_BEGIN});
    fetch('http://www.mocky.io/v2/5b7e4266300000350084c089')
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw 'error';
        }
      })
      .then(users => {
          this.dispatch({type: `${ActionTypes.GET_USERS}${AsyncActionTypes.LOAD_USER_SUCCESS}`, payload: users});
        }
      )
      .catch(error => {
        this.dispatch({type: AsyncActionTypes.LOAD_USER_FAILURE, payload: error});
      });
    };

  setUnitId = (id: string) => {
    this.dispatch({type: ActionTypes.SET_CURRENT_UNIT, payload: id})
  };

  setUserId = (id: string) => {
    this.dispatch({type: ActionTypes.SET_CURRENT_USER, payload: id})
};

  setOrgId = (id: string) => {
    this.dispatch({type: ActionTypes.SET_CURRENT_ORG, payload: id})
  };

  saveOrg = (account: IAccount) => {
    this.dispatch({type: ActionTypes.SAVE_ORG, payload: account});
    this.dispatch({type: ActionTypes.INCREMENT_COUNTER});
  };

  editOrg = (organization: IAccount) => {
    this.dispatch({type: ActionTypes.EDIT_ORG, payload: organization});
  };

  deleteOrg = (organization: IAccount) => {
    this.dispatch({type: ActionTypes.DELETE_ORG, payload: organization});
  };

  saveUnit = (unit: IUnit) => {
    this.dispatch({type: ActionTypes.SAVE_UNIT, payload: unit});
    this.dispatch({type: ActionTypes.INCREMENT_COUNTER});
  };

  deleteUnit = (unit: IUnit) => {
    this.dispatch({type: ActionTypes.DELETE_UNIT, payload: unit});
  };

  editUnit = (unit: IUnit) => {
    this.dispatch({type: ActionTypes.EDIT_UNIT, payload: unit});
  };

  saveUser = (user: IUser) => {
    this.dispatch({type: ActionTypes.SAVE_USER, payload: user});
    this.dispatch({type: ActionTypes.INCREMENT_COUNTER});
  };

  deleteUser = (user: IUser) => {
    this.dispatch({type: ActionTypes.DELETE_USER, payload: user});
  };

  editUser = (user: IUser) => {
    this.dispatch({type: ActionTypes.EDIT_USER, payload: user});
  }

}
