import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import Routes from './Routes/Routes'
import {appStore} from './Store/Store';

ReactDOM.render(
  <Provider store={appStore}>
    <Routes/>
  </Provider>,
  document.getElementById('app')
);
