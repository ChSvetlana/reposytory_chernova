import * as React from 'react';
import * as ReactModal from 'react-modal';
import {connect, Dispatch} from 'react-redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {IUser} from './User';

interface IProps {
  actions: Actions;
  isOpen: boolean;
  onRequestClose: () => void;
  className: string;
  overlayClassName: string;
  isEdit: boolean;
  user: IUser;
  unitId: string;
}

interface IStateProps {
  counter: number;
}

interface IState {
  fio: string;
  address: string;
  job: string;
}

const initialState: IState = {
  fio: '',
  address: '',
  job: '',
};

class UserModalForm extends React.Component<IProps & IStateProps, IState> {

  state: IState = initialState;

  changeFioUser = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({fio: e.currentTarget.value})
  };

  changeJobUser = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({job: e.currentTarget.value});
  };

 changeAddressUser = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({address: e.currentTarget.value});
  };

  handleSaveModal = () => {
    const {user, isEdit,unitId} = this.props;
    const  newUser = {
      ...this.state,
      id: isEdit ? user.id : (this.props.counter + 1).toString(),
      unitId: unitId,
    };

    if (isEdit) {
      this.props.actions.editUser(newUser);
    } else {
      this.props.actions.saveUser(newUser);
  }

    this.setState({...initialState});
    this.props.onRequestClose();
  };

  componentWillReceiveProps(newProps: IProps) {
    if (newProps.isEdit) {
      const {fio, address, job} = newProps.user;
      this.setState({
        fio,
        address,
        job
      })
    }
  };

  render() {
    const {fio, address, job} = this.state;

    return (
        <ReactModal
          {...this.props}
        >
          <p>Добавление новой организации</p>
          <input type="text" placeholder= "Введите название поздразделения" value={fio} onChange={this.changeFioUser}/>
          <input type="text" placeholder= "Введите адрес" value={address} onChange={this.changeAddressUser}/>
          <input type="text" placeholder= "Введите телефон" value={job} onChange={this.changeJobUser}/>
          <button onClick={this.props.onRequestClose}>Закрыть</button>
          <button onClick={this.handleSaveModal}>Сохранить</button>
        </ReactModal>
    );
  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    counter: state.counter,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectedUserModalForm = connect(mapStateToProps, mapDispatchToProps)(UserModalForm);

export {connectedUserModalForm as UserModalForm};
