import * as React from 'react';
import * as ReactModal from 'react-modal';
import {connect, Dispatch} from 'react-redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {IUnit} from './Unit';

interface IProps {
  actions: Actions;
  isOpen: boolean;
  onRequestClose: () => void;
  className: string;
  overlayClassName: string;
  isEdit: boolean;
  unit: IUnit;
  orgId: string;
}

interface IStateProps {
  counter: number;
}

interface IState {
  name: string;
  telefon: string;
}

const initialState: IState = {
  name: '',
  telefon: '',
};

class UnitModalForm extends React.Component<IProps & IStateProps, IState> {

  state: IState = initialState;

  changeNameUnit = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({name: e.currentTarget.value})
  };

  changeTelefonUnit = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({telefon: e.currentTarget.value});
  };

  handleSaveModal = () => {
    const {unit, isEdit,orgId} = this.props;
    const  newUnit = {
      ...this.state,
      id: isEdit ? unit.id : (this.props.counter + 1).toString(),
      orgId: orgId,
    };

    if (isEdit) {
      this.props.actions.editUnit(newUnit);
    } else {
      this.props.actions.saveUnit(newUnit);
    }

    this.setState({...initialState});
    this.props.onRequestClose();
  };

  componentWillReceiveProps(newProps: IProps) {
    if (newProps.isEdit) {
      const {name, telefon} = newProps.unit;
      this.setState({
        name,
        telefon,
      })
    }
  };

  render() {
    const {name, telefon} = this.state;

    return (
        <ReactModal
          {...this.props}
        >
          <p>Добавление новой организации</p>
          <input type="text" placeholder= "Введите название поздразделения" value={name} onChange={this.changeNameUnit}/>
          <input type="text" placeholder= "Введите телефон" value={telefon} onChange={this.changeTelefonUnit}/>
          <button onClick={this.props.onRequestClose}>Закрыть</button>
          <button onClick={this.handleSaveModal}>Сохранить</button>
          </ReactModal>
    );
  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    counter: state.counter,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectedUnitModalForm = connect(mapStateToProps, mapDispatchToProps)(UnitModalForm);

export {connectedUnitModalForm as UnitModalForm};
