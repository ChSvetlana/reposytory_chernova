import * as React from 'react';
import * as ReactModal from 'react-modal';
import {connect, Dispatch} from 'react-redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {IAccount} from './Account';

interface IProps {
  actions: Actions;
  isOpen: boolean;
  onRequestClose: () => void;
  className: string;
  overlayClassName: string;
  isEdit: boolean;
  organization: IAccount;
}

interface IStateProps {
  counter: number;
}

interface IState {
  name: string;
  address: string;
  inn: string;
}
const initialState: IState = {
  name: '',
  address: '',
  inn: ''
};

class OrgModalForm extends React.Component<IProps & IStateProps, IState> {

  state: IState = initialState;

  componentWillReceiveProps(newProps: IProps) {
    if (newProps.isEdit) {
      const {name, address, inn} = newProps.organization;
      this.setState({
        name,
        address,
        inn,
      })
    }
  };

  changeNameOrg = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({name: e.currentTarget.value})
  };

  changeaddressOrg = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({address: e.currentTarget.value});
  };

  changeinnOrg = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({inn: e.currentTarget.value});
  };

  handleSaveModal = () => {
    const {organization, isEdit} = this.props;

    const newOrganization = {
      ...this.state, /*name: name: this.state.name, и остальные длинные присваивания заменяем на эту короткуб строчку*/
      id: isEdit ? organization.id : (this.props.counter + 1).toString(),
    };
    if (isEdit) {
      this.props.actions.editOrg(newOrganization);
    } else {
      this.props.actions.saveOrg(newOrganization);
    }
    this.setState({...initialState});
    this.props.onRequestClose();
  };

  render() {
    const {name, address, inn} = this.state;

    return (
        <ReactModal
          {...this.props}
        >
          <p>Добавление новой организации</p>
          <input type="text" placeholder= "Введите название организации" value={name} onChange={this.changeNameOrg}/>
          <input type="text" placeholder= "Введите адрес организации" value={address} onChange={this.changeaddressOrg}/>
          <input type="text" placeholder= "Введите ИНН организации" value={inn} onChange={this.changeinnOrg}/>
          <button onClick={this.props.onRequestClose}>Закрыть</button>
          <button onClick={this.handleSaveModal}>Сохранить</button>
        </ReactModal>
    );
  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    counter: state.counter,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectedOrgModalForm = connect(mapStateToProps, mapDispatchToProps)(OrgModalForm);

export {connectedOrgModalForm as OrgModalForm};
