import * as React from 'react';
import {Actions} from '../Actions/Actions';

interface IAppProps {
  loginStatus: string;
  passwordValue: string;
  loading: boolean;
  isCorrectLogin: boolean;
  loginValue: string;
  handleLogout: any;
  changeHandlerLogin: any;
  changeHandlerPassword: any;
  handleSubmit: any;
  goToAccountPage: any;
  actions: Actions;
  openUnit: any;
  error: string;
}

function ContainerApp(props: IAppProps) {
    const {loginValue, loading,loginStatus, isCorrectLogin, handleSubmit, passwordValue,error, handleLogout} = props;
    const renderAuthorization =  () => loginStatus ?
      <div>
    {`Вы авторизованы как ${loginStatus} `}
        <a onClick={handleLogout} href={void 0}>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512">
            <path d="M384 320v-64h-160v-64h160v-64l96 96zM352 288v128h-160v96l-192-96v-416h352v160h-32v-128h-256l128 64v288h128v-96z"></path>
          </svg>
        </a>
      </div> :
      <form className="col-md-4" onSubmit={handleSubmit}>
        <input className="input-login" type="text" placeholder="Введите логин" value={loginValue} onChange={props.changeHandlerLogin}/>
        <input className="input-password" type="text" placeholder="Введите пароль" value={passwordValue} onChange={props.changeHandlerPassword}/>
        <a onClick={handleSubmit} href={void 0}>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512">
            <path d="M192 256h-160v-64h160v-64l96 96-96 96zM512 0v416l-192 96v-96h-192v-128h32v96h160v-288l128-64h-288v128h-32v-160z"></path>
          </svg>
        </a>
      </form>

    return (
      <div>
        <h3>
            Задание по отображению информации по организациям
        </h3>
        {
          renderAuthorization()
        }
        {
          !isCorrectLogin && !loading && <p>Некорректный логин. Попробуйте еще раз</p>
        }
        {
          loading && <h4>Идет загрузка...</h4>
        }
        {
          loginStatus ?
            <div>
              <input className="btn btn-outline-warning" disabled={loading} type="button" value="Просмотр организаций" onClick={props.goToAccountPage}/>
            </div> :
            <div className= "col-md-12">
              {error}
            </div>
        }
      </div>
    );
  }

export default ContainerApp;
