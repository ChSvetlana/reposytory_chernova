import * as React from 'react';
import {connect} from 'react-redux';
import  {Link} from 'react-router-dom'
import {Dispatch} from 'redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {ConfirmDeletionModalForm} from './ConfirmDeletionModalForm';
import {IUnit} from './Unit';
import {UserModalForm} from './UserModalForm';

export interface IUser {
  id: string;
  fio: string;
  address: string;
  job: string;
  unitId: string;
}

interface IProps {
  actions: Actions;
}

interface IStateProps {
  users?: Array<IUser>;
  unitId: string;
  units: Array<IUnit>;
}

interface IState {
  showModal: boolean;
  showConfirmDeletionModal: boolean;
  curUser: IUser;
  confirmCallback: () => void;
  editing: boolean,
}

class User extends React.Component<IProps & IStateProps> {

  state: IState = {
   showModal: false,
   showConfirmDeletionModal: false,
   curUser: null,
   confirmCallback: null,
   editing: false,
  };

  componentWillMount() {
   this.props.actions.getUsers();
  };

  static contextTypes = {
   history: React.PropTypes.array
  };

  handleCloseModal = () => {
   this.setState({
    showModal: false,
    curUnit: null,
    editing: false
   })
  };

  createUser = () =>  {
   this.setState({showModal: true,
    editing: false})
  };

  handleCloseConfirmDeletionModal = () => {
   this.setState({showConfirmDeletionModal: false});
  };

  deleteUser = (user: IUser) => {
   this.setState({
    showConfirmDeletionModal: true,
    curUser: user,
    confirmCallback: () => {
      this.props.actions.deleteUser(user);
      this.setState({showConfirmDeletionModal: false,  curUser: null});
    }
   });
  };

  editUser = (user: IUser) => {
   this.setState({
    showModal: true,
    editing: true,
    curUser: user,
   })
  };

  handleLogout = () => {
   const {actions} = this.props;
   actions.onLogout();
   this.context.history.push(`/`);
  };

  renderUsers() {
   const {users, unitId} = this.props;
   const rows: Array<any> = [];

   users.forEach(function (user) {
    if (user.unitId === unitId){
    rows.push(
      <tr key ={'user' + user.id}>
       <td>{user.id}</td>
       <td >{user.fio}</td>
       <td>{user.address}</td>
       <td>{user.job}</td>
       <td>{user.unitId}</td>
       <td><input type="button" value="Удалить" onClick ={() => {
        this.deleteUser(user)
       }}/></td>
       <td><input type="button" value="Изменить" onClick ={() => {
        this.editUser(user)
       }}/></td>
      </tr>
    ) }
   }, this);

   return rows;
  }

  render() {
   const {users,units,unitId} = this.props;
   const {showConfirmDeletionModal, showModal, confirmCallback, curUser, editing} = this.state;

   const unitName = units.filter((unit) => unit.id === unitId)[0].name;
   return (
    <div>
      <h3>
       {`Пользователи подразделения ${unitName}`}
      </h3>
    <a onClick={this.handleLogout} href={void 0}>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512">
      <path d="M384 320v-64h-160v-64h160v-64l96 96zM352 288v128h-160v96l-192-96v-416h352v160h-32v-128h-256l128 64v288h128v-96z"></path>
    </svg>
    </a>
      <table className="table">
       <tbody>
       <tr>
        <td>Идентификатор</td>
        <td>ФИО</td>
        <td>Адрес</td>
        <td>Должность</td>
        <td>Идентификатор организации</td>
       </tr>
       {
        users && this.renderUsers()
       }
       </tbody>
      </table>
      <input className="btn btn-outline-primary" type="button" value="Добавить пользователя" onClick={this.createUser}/>
    <Link  to= "/units">Назад</Link>
      <UserModalForm
       isEdit={editing}
       isOpen={showModal}
       onRequestClose={this.handleCloseModal}
       className="Modal"
       overlayClassName="Overlay"
       user={curUser}
       unitId={unitId}
      />
      <ConfirmDeletionModalForm
       isOpen={showConfirmDeletionModal}
       onRequestClose={this.handleCloseConfirmDeletionModal}
       className="Modal"
       overlayClassName="Overlay"
       callback={confirmCallback}
      />
    </div>
   );
  };

}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
   users: state.users,
   units: state.units,
   unitId: state.unitId,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
   actions: new Actions(dispatch)
  };
}

User.contextTypes = {
  history: React.PropTypes.object
};

const connectedUser = connect(mapStateToProps, mapDispatchToProps)(User);

export {connectedUser as User};
