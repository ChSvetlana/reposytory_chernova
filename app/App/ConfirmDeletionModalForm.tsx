import * as React from 'react';
import * as ReactModal from 'react-modal';

interface IProps {
  isOpen: boolean;
  onRequestClose: () => void;
  className: string;
  overlayClassName: string;
  callback: () => void;
}

export function ConfirmDeletionModalForm(props: IProps) {

    return <ReactModal
          {...props}
        >
          <p>Вы действительно хотите удалить  запись?</p>
          <button onClick={props.callback}>Да</button>
          <button onClick={props.onRequestClose}>Нет</button>
        </ReactModal>
}
