import * as React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Dispatch} from 'redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {IAccount} from './Account';
/*import {Account} from './Account';*/
import {ConfirmDeletionModalForm} from './ConfirmDeletionModalForm';

/*import NotFound from './NotFound';*/
import {UnitModalForm} from './UnitModalForm';

export interface IUnit {
  id: string;
  name: string;
  telefon: string;
  orgId: string;
}

interface IProps {
  actions: Actions;
}

export interface IStateProps {
  units?: Array<IUnit>;
  orgId: string;
  accounts: IAccount[];
}

interface IState {
    showModal: boolean;
    showConfirmDeletionModal: boolean;
    curUnit: IUnit;
    confirmCallback: () => void;
    editing: boolean,
    route: string,
}

class Unit extends React.Component<IProps & IStateProps> {

    state: IState = {
    showModal: false,
    showConfirmDeletionModal: false,
    curUnit: null,
    confirmCallback: null,
    editing: false,
    route: window.location.hash.substr(1),
 };

    createUnit = () => {
    this.setState({
        showModal: true,
        editing: false})
    };

    componentWillMount() {
    this.props.actions.getUnits();
  }
  componentDidMount() {
     window.addEventListener('hashchange', () =>
      {
       this.setState({route: window.location.hash.substr(1)})
      })
  }
    static contextTypes = {
        history: React.PropTypes.array
    };

    handleCloseModal = () => {
        this.setState({
            showModal: false,
            curOrg: null,
            editing: false
        })
    };

    handleCloseConfirmDeletionModal = () => {
        this.setState({showConfirmDeletionModal: false});
    };

  handleUnitOnClick = (id: string) => {
  this.props.actions.setUnitId(id);
  this.context.history.push(`/users/${id}`);
  };

  deleteUnit = (unit: IUnit) => {
        this.setState({
            showConfirmDeletionModal: true,
            curOrg: unit,
            confirmCallback: () => {
                this.props.actions.deleteUnit(unit);
                this.setState({showConfirmDeletionModal: false,   curUnit: null});
            }
        });
    };

    editUnit = (unit: IUnit) => {
        this.setState({
            showModal: true,
            editing: true,
            curUnit: unit,
        })
    };

  handleLogout = () => {
    const {actions} = this.props;
    actions.onLogout();
    this.context.history.push(`/`);
  };

  renderUnits() {
    const {units, orgId} = this.props;
    const rows: Array<any> = [];

    units.forEach(function (unit) {
      if (orgId === unit.orgId) {
        rows.push(
            <tr key={'unit' + unit.id} >
            <td>{unit.id}</td>
                        <td onClick={() => {
                            this.handleUnitOnClick(unit.id)
                            }}>{unit.name}</td>
            <td>{unit.telefon}</td>
            <td>{unit.orgId}</td>
                        <td><input type="button" value="Удалить" onClick ={() => {
                            this.deleteUnit(unit)
                        }}/></td>
                        <td><input type="button" value="Изменить" onClick ={() => {
                            this.editUnit(unit)
                        }}/></td>
        </tr>
        )
      }
    }, this);

    return rows;
  }

  render() {
    const {units, accounts,orgId} = this.props;
        const {showConfirmDeletionModal, showModal, confirmCallback, curUnit, editing} = this.state;

        const organizationName = accounts.filter((org) => org.id === orgId)[0].name;

 /* let Child;
  switch (this.state.route)
   {
      case '/accounts': Child = Account; break;
      default: Child = NotFound;
   }
*/
    return (
      <div>
        <h3>
                    {`Подразделения организации ${organizationName}`}
        </h3>
        <a onClick={this.handleLogout} href={void 0}>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512">
            <path d="M384 320v-64h-160v-64h160v-64l96 96zM352 288v128h-160v96l-192-96v-416h352v160h-32v-128h-256l128 64v288h128v-96z"></path>
          </svg>
        </a>
        <table className="table">
          <tbody>
          <tr>
            <td>Идентификатор</td>
            <td>Название</td>
            <td>Телефон</td>
            <td>Идентификатор организации</td>
          </tr>
          {
                        units && this.renderUnits()
          }
          </tbody>
        </table>
        <input className="btn btn-outline-primary" type="button" value="Добавить подразделение" onClick={this.createUnit}/>
        <Link  to= "/accounts">Назад</Link>
               <UnitModalForm
                    isEdit={editing}
                    isOpen={showModal}
                    onRequestClose={this.handleCloseModal}
                    className="Modal"
                    overlayClassName="Overlay"
                    unit={curUnit}
                    orgId={orgId}
                />
                <ConfirmDeletionModalForm
                    isOpen={showConfirmDeletionModal}
                    onRequestClose={this.handleCloseConfirmDeletionModal}
                    className="Modal"
                    overlayClassName="Overlay"
                    callback={confirmCallback}
                />
      </div>
    );
  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    units: state.units,
        orgId: state.orgId,
        accounts: state.accounts
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

Unit.contextTypes = {
    history: React.PropTypes.object
};

const connectedUnit = connect(mapStateToProps, mapDispatchToProps)(Unit);

export {connectedUnit as Unit};
