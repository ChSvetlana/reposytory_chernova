// Задание на первую неделю
//
// минимум:
//     1. Учетка в БитБакет
// 2. Поставить все вспомогательные инструменты (NodeJs, Git, и т.п.)
// 3. Разобраться с бойлерплейтом (добиться его работоспособности)
//
// желательно:
//     4. Создать реактовскую страничку для ввода логина и реакцией на нажатия кнопок (можно в консоли)
//
// Было бы круто:
//     5. При нажатии кнопки "войти" на странице логина прокинуть Экшен через стор и сохранить в сторе признак, что пользователь зарегистрирован. После этого отобразить страничку с кнопкой выход. При нажатии "выход", прокинтуть через стор признак выхода
// в качестве стилефого фреймворка использовать Bootstrap

import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {LOGINS} from '../Actions/Consts';
import {IStoreState} from '../Store/Store';
import './index.css';
import ContainerApp from './ComponentApp';
// import {PropTypes} from 'react';

interface IStateProps {
  loginStatus: string;
  loading: boolean;
  error: string;
}

interface IState {
  loginValue: string;
  passwordValue: string;
  isCorrectLogin: boolean;
}

type TProps = IDispatchProps & IStateProps;

class App extends React.Component<TProps, IState> {

   constructor(props: any) {
   super(props);
   this.handleSubmit = this.handleSubmit.bind(this)
   }

  static contextTypes = {
    history: React.PropTypes.array
  };

  state: IState = {
    loginValue: null,
    passwordValue: null,
    isCorrectLogin: true,

  };

  handleSubmit = (e: React.SyntheticEvent<HTMLButtonElement>) => {  /* ф-ция для кн. Войти*/
      e.preventDefault(); /*// отмена отправки данных на сервер после нажатия кнопки "Войти"*/
      this.handleLogin();
    };

  handleLogin = () => {
    const {actions} = this.props;
        if ( this.verifyLogin() &&  this.state.loginValue) {
            this.setState({isCorrectLogin: true});
            actions.onLogin(this.state.loginValue, this.state.passwordValue);
        }
        else {
            this.setState({isCorrectLogin: false});
        }
  };
  changeHandlerLogin = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({loginValue: e.currentTarget.value});
   };

  changeHandlerPassword = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({passwordValue: e.currentTarget.value});
  };

  verifyLogin = () => {
    return LOGINS.some((login) => this.state.loginValue === login);
    };

  goToAccountPage = () => {
    this.context.history.push('/accounts');
  };

  handleLogout = () => {
    const {actions} = this.props;
    actions.onLogout();
  };

  openUnit = () => {
    this.context.history.push('/units');
  };

  render () {

    const {loginStatus, loading, actions, error} = this.props;
    const {isCorrectLogin, loginValue, passwordValue} = this.state;
    return <div>
              <ContainerApp
                loginStatus={loginStatus}
                passwordValue={passwordValue}
                loading={loading}
                loginValue={loginValue}
                isCorrectLogin={isCorrectLogin}
                handleLogout={this.handleLogout}
                changeHandlerLogin={this.changeHandlerLogin}
                changeHandlerPassword={this.changeHandlerPassword}
                handleSubmit={this.handleSubmit}
                goToAccountPage={this.goToAccountPage}
                actions={actions}
                openUnit={this.openUnit}
                error={error}
              />
          </div>

  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    loginStatus: state.loginStatus,
    loading: state.loading,
    error: state.error,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
    actions: new Actions(dispatch)
  };
}

App.contextTypes = {
  history: React.PropTypes.object
};

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
