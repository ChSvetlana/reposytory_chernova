import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';
import {ConfirmDeletionModalForm} from './ConfirmDeletionModalForm';
import {OrgModalForm} from './OrgModalForm';

export interface IAccount {
  id: string;
  name: string;
  address: string;
  inn: string;
}

interface IProps {
  actions: Actions;
}

interface IStateProps {
  accounts?: Array<IAccount>;
}

interface IState {
  showModal: boolean;
  showConfirmDeletionModal: boolean;
  curOrg: IAccount;
  confirmCallback: () => void;
  editing: boolean
}

class Account extends React.Component<IProps & IStateProps, IState> {

  state: IState = {
    showModal: false,
    showConfirmDeletionModal: false,
    curOrg: null,
    confirmCallback: null,
    editing: false,
  };

  componentWillMount() {
    this.props.actions.getAccounts();
  };

 static contextTypes = {
    history: React.PropTypes.array
  };

  openUnit = () => {
   this.context.history.push('/units/');
  };

  handleCloseModal = () => {
    this.setState({
      showModal: false,
      curOrg: null,
      editing: false
    })
  };

  handleLogout = () => {
     const {actions} = this.props;
    actions.onLogout();
    this.goToLogin();
  };

   goToLogin = () => {
      this.context.history.push (`/`);
   };

   createOrganization = () => {
    this.setState({
      showModal: true,
      editing: false
    })
  };

  handleOrgOnClick = (id: string) => {
    this.props.actions.setOrgId(id);
    this.context.history.push(`/units/${id}`);
  };

  deleteOrganization = (organization: IAccount) => {
      this.setState({
        showConfirmDeletionModal: true,
        curOrg: organization,
        confirmCallback: () => {
              this.props.actions.deleteOrg(organization);
              this.setState({showConfirmDeletionModal: false,   curOrg: null});
          }
      });
  };

  editOrganization = (organization: IAccount) => {
    this.setState({
      showModal: true,
      editing: true,
      curOrg: organization
    })
  };

  handleCloseConfirmDeletionModal = () => {
    this.setState({showConfirmDeletionModal: false});
  };

  back = () => {
    alert ('work');
    };

  renderAccounts() {
  const {accounts} = this.props;
  const rows: Array<any> = [];
  accounts.forEach(function (organization) {
    rows.push(
      <tr  key={'org' + organization.id}>
        <td>{organization.id}</td>
        <td onClick={() => {
          this.handleOrgOnClick(organization.id);
        }}>{organization.name}</td>
        <td>{organization.address}</td>
        <td>{organization.inn}</td>
        <td><input type="button" value="Удалить" onClick ={() => {
          this.deleteOrganization(organization)
        }}/></td>
        <td><input type="button" value="Изменить" onClick ={() => {
          this.editOrganization(organization)
        }}/></td>
      </tr>
    )
  }, this);

  return rows;
}

  render() {
    const {accounts} = this.props;
    const {showConfirmDeletionModal, showModal, confirmCallback, curOrg, editing} = this.state;
    return (
      <div>

        <h3>
          Данные об организациях
        </h3>
        <a onClick={this.handleLogout} href={void 0}>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512">
            <path d="M384 320v-64h-160v-64h160v-64l96 96zM352 288v128h-160v96l-192-96v-416h352v160h-32v-128h-256l128 64v288h128v-96z"></path>
          </svg>
        </a>
        <table className="table">
          <tbody>
          <tr>
            <td>Идентификатор</td>
            <td>Название</td>
            <td>Адрес</td>
            <td>ИНН</td>
          </tr>
          {
            accounts && this.renderAccounts()
          }
          </tbody>
        </table>
        <input className="btn btn-outline-primary" type="button" value="Добавить организацию" onClick ={this.createOrganization}/>
        <OrgModalForm
          isEdit={editing}
          isOpen={showModal}
          onRequestClose={this.handleCloseModal}
          className="Modal"
          overlayClassName="Overlay"
          organization={curOrg}
        />
        <ConfirmDeletionModalForm
          isOpen={showConfirmDeletionModal}
          onRequestClose={this.handleCloseConfirmDeletionModal}
          className="Modal"
          overlayClassName="Overlay"
          callback={confirmCallback}
        />
      </div>
    );
  };
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    accounts: state.accounts,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

Account.contextTypes = {
  history: React.PropTypes.object
};

const connectedAccounts = connect(mapStateToProps, mapDispatchToProps)(Account);

export {connectedAccounts as Account};
