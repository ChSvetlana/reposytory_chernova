import * as React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
    <div>
        <h1>Ресурс не найден (Ошибка 404).</h1>
        <Link to="/">Вернуться на главную</Link>
    </div>
);

export default NotFound;
